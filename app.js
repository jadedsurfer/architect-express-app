// Configures the core express server
"use strict";

var express = require("express");

module.exports = function (options, imports, register) {

  var debugExpress = imports.debug('express:app');
  var debugHttp = imports.debug('http');
  debugExpress("start");

  var hub = imports.hub;

  // Create base app
  var app = express();


  // Enhance app api

  // Allow access to the standard express module
  // primarily for creating new apps
  app.getModule = function () {
    return express;
  };

  // Shortcut to create a new router (preferred method for creating sub apps)
  app.Router = express.Router.bind(express);

  // Shortcut to static middleware (another reason the express module might be needed directly)
  app.static = express.static.bind(express);

  app.startServer = function (port, host) {

    var actualPort = port || options.port;
    var actualHost = host || options.host || null;

    app.getPort = function () {
      return actualPort;
    };
    app.getHost = function () {
      return actualHost;
    };

    if (actualHost) {
      app.listen(actualPort, actualHost, listenCallback);
    } else {
      app.listen(actualPort, listenCallback);
    }

    function listenCallback(err) {
      if (err) {
        debugHttp("error starting express app at http://" + actualHost + ":" + actualPort);
        process.exit(1);
      } else {
        debugHttp("express app listening at http://" + actualHost + ":" + actualPort);
        return app;
      }
    }
  };


  // Setup hook servers to help with ordering middleware and routers

  // if a hook early in the chain doesn't have functions, the server will throw an error
  // TODO: figure out how to avoid the error
  var hookNames = [
    "Setup",
    "Security",
    "Apps",
    "Static",
    "Error"
  ];

  // Set up hooks to add middleware and routes in a particular order
  hookNames.forEach(function (name) {
    debugExpress("set up hook server: %s", name);
    var hookServer = express();
    app.use(hookServer);
    app["use" + name] = hookServer.use.bind(hookServer);
  });

  hub.on("start", function(options){
    options = options || {};
    var port = options.port || null;
    var host = options.host || null;
    app.startServer(port, host);
  });


  debugExpress("register express");
  register(null, {
    "onDestruct": function (callback) {
      app.close();
      app.on("close", callback);
    },
    "express": app,
    "routeServer": app
  });

};

